<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['success_del'] = "Запись успешно удалена";
$lang['success_edit'] = "Запись успешно изменина";
$lang['success_add'] = "Запись успешно добавлена";
$lang['error_name'] = "Название не валидно";
$lang['error_content'] = "Содержание не валидно";
$lang['error_aut'] = "Пароль или логин не правильные";
$lang['error_correct'] = "Не корректные данные";

