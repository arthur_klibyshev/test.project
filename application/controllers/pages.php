<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function start($page = 'home') {
		$data['title'] = ucfirst($page);
		$data['array'] = array(1, 2, 3, 'aaaa', 'ffff');

		
		$this->load->view('pages/'.$page, $data);
	}


}