<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {


    public function __construct() {

        parent::__construct();
        $this->load->model('Users_model', "", TRUE);
        $this->load->library('session');
        $this->load->helper('url');

    }
    public function registration() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_name', 'User name', 'trim|required|min_length[3]|max_length[50]|callback_name_check');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[50]|callback_pass_check');
        $this->form_validation->set_rules('pass_confirm', 'Password confirmation', 'callback_pass_confirm');
        $this->form_validation->set_rules('first_name', 'First name', 'trim|required|min_length[2]|max_length[20]|alpha');
        $this->form_validation->set_rules('last_name', 'Last name', 'trim|required|min_length[2]|max_length[20]|alpha');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[6]|max_length[320]|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Registration form';
            $data['route'] = 'sites/registration';
            $data['user_name'] = set_value('user_name');
            $data['password'] = set_value('password');
            $data['pass_confirm'] = set_value('pass_confirm');
            $data['first_name'] = set_value('first_name');
            $data['last_name'] = set_value('last_name');
            $data['email'] = set_value('email');

            $this->load->view('sites/registration', $data);
        } else {
            $data = array(
                'user_name' => $this->input->post('user_name'),
                'url' => $this->input->post('url'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'password' => sha1(md5($this->input->post('password')).$this->config->item('encryption_key')),
                'role' => USER_ROLE,
                'banned' => 0
            );

            if (!$this->Users_model->add_user($data)) {
                show_error('Database error!');
                exit;
            }
            $newdata = array(
                'first_name'  => $data['first_name'],
                'last_name' => $data['last_name'],
                'role' => USER_ROLE
            );

            $this->session->set_userdata($newdata);
            $this->session->set_flashdata('msg', 'You have successfully registered!');
            redirect('sites');
            exit;
        }
    }
    public function login() {
        $this->load->helper('form');
        $this->load->library('mycurl');
        $this->lang->load('messages');

        if($this->session->userdata('user_authorization') == true){
            redirect('/');
            exit;
        }

        $message='';
        $params = array(
            'redirect_uri'  => $this->config->item('redirect_uri'),
            'response_type' => 'code',
            'client_id'     => $this->config->item('client_id'),
            'scope'         => $this->config->item('scope')
        );

        $link = $this->config->item('auth_url')  . '?' . urldecode(http_build_query($params));

        if (!empty($this->session->userdata('code'))){

            $params = array(
                'client_id'     => $this->config->item('client_id'),
                'redirect_uri'  => $this->config->item('redirect_uri'),
                'client_secret' => $this->config->item('client_secret'),
                'grant_type'    => 'authorization_code',
                'code'          => $this->session->userdata('code')
            );

            $token = json_decode($this->mycurl->curlBegin($this->config->item('token_url'),$params),true);

            if (isset($token['access_token'])){

                $params['access_token'] = $token['access_token'];
                $userInfo = json_decode($this->mycurl->curlBegin('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))),true);

                if (isset($userInfo['id'])) {

                    $user_data = $this->Users_model->get_users("u.email = '". $userInfo['email'] ."'");

                    if(empty($user_data)){
                        $user_data = array('user_name' => $userInfo['email'],
                            'first_name' => $userInfo['given_name'],
                            'last_name' => $userInfo['family_name'],
                            'email' => $userInfo['email'],
                            'password' => sha1(md5($userInfo['given_name'].$userInfo['email']).$this->config->item('encryption_key')),
                            'role' => USER_ROLE,
                            'banned' => 0);
                        if ($this->Users_model->add_user($user_data) == FALSE) {
                            show_error('Database error!');
                            exit;
                        }

                    }

                    $this->session->set_userdata('user_data',$user_data);
                    $this->session->set_userdata('user_authorization',true);
                    redirect('/');
                    exit;
                }
                $message = $this->lang->line('error_aut').'<br/>';
            }
        }

        if(!empty($this->input->post('user_name')) && !empty($this->input->post('password'))){

            if (!preg_match('|^[\w\s_+]+$|',$this->input->post('user_name')) || !preg_match('|^[\w()\s_+]+$|',$this->input->post('password'))){

                $message = $this->lang->line('error_correct').'<br/>';
            } else {

                $data = $this->Users_model->get_users('u.user_name = '. $this->db->escape($this->input->post('user_name')) ." AND
                                                       u.password = '". sha1(md5($this->input->post('password')).$this->config->item('encryption_key')) ."'");

                if($data == false){
                     $message = $this->lang->line('error_aut').'<br/>';
                } else {
                    $this->session->set_userdata('user_data',$data);
                    $this->session->set_userdata('user_authorization',true);
                    redirect('/');
                    exit;
                }
            }
        }
        $data['title'] = 'Login page';
        $data['message'] = $message;
        $data['link'] = $link;
        $this->load->view('sites/login',$data);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/');
        exit;
    }

    public function pass_confirm($str) {
        if ($this->input->post('password') != $str) {
            $this->form_validation->set_message('pass_confirm', 'Passwords must be equal!');
            return FALSE;
        }
        return TRUE;
    }

    public function name_check($str) {
        if (!preg_match("/^[\s\w\.]+$/", $str)) {
            $this->form_validation->set_message('name_check', 'Name cannot contain special characters!');
            return FALSE;
        }
        return TRUE;
    }

    public function pass_check($str) {
        if (!preg_match("/^[\w\d]+$/", $str)) {
            $this->form_validation->set_message('pass_check', 'Password cannot contain special characters!');
            return FALSE;
        }
        return TRUE;
    }
}