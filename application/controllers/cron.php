<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct(){
		parent::__construct();
        if (!$this->input->is_cli_request()){
            echo "Error. It is not a CLI request!";
			exit;
        }
	}

	public function request() {
		$this->load->model('Sites_model', "", TRUE);
		$sites = $this->Sites_model->get_sites();
		if (empty($sites)) {
			echo "No sites in the database!";
			exit;
		}
		$this->load->library('mycurl');
		foreach ($sites as $site) {
			$req = $site->url;
			$result = $this->mycurl->curlBegin($req, 'api='.$site->apikey, TRUE, FALSE, FALSE);
			$output = reset($result);
            $status = end($result);
			if ($status != SUCCESS_RES && $output !== 1) {
				$string = 'Status - ' . $status . " An error has occured!";
				$msgtype = 'error';
			} else {
				$string = 'Status - ' . $status . " Success!";
				$msgtype = 'info';
			}
			log_message($msgtype, $string);
		}
	}
}