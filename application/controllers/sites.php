<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sites extends BASE_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('Sites_model', "", TRUE);
        $this->load->helper('url');
        $this->load->library('session');

        if ($this->session->userdata('user_data')->role_id != ADMIN_ROLE){
            show_404();
            exit;
        }
    }

    public function showsites() {

        $data['sites'] = $this->Sites_model->get_sites();
        $data['title'] = 'Sites list';

        $data['usermsg'] = $this->session->userdata('user_data');
        $this->load->view('sites/main', $data);

    }

    public function addsite() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('mylib');

        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[1]|max_length[60]|callback_name_check');
        $this->form_validation->set_rules('url', 'Url', 'trim|required|min_length[5]|max_length[255]|callback_url_check');

        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Add form';
            $data['route'] = 'sites/add';
            $data['label1'] = 'Site name';
            $data['label2'] = 'Url';
            $data['name'] = set_value('name');
            $data['url'] = set_value('url');
            $this->load->view('sites/addform', $data);
        } else {
            $data = array('name' => $this->input->post('name'),
                          'url' => $this->input->post('url'),
                          'apikey' => $this->mylib->randomKey());

            if (!$this->Sites_model->add_site($data))
                show_error('Database error!');

            $this->session->set_flashdata('msg', 'Site has added to DB');
            redirect('sites');
        }
    }

    public function deletesite($id) {
        if (!$this->Sites_model->delete_site($id))
            show_error('Database error!');
        $this->session->set_flashdata('msg', "Site $id has deleted from DB");
        redirect('sites');
    }

    public function editsite($id) {

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('mylib');

        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[1]|max_length[60]|callback_name_check');
        $this->form_validation->set_rules('url', 'Url', 'trim|required|min_length[5]|max_length[255]|callback_url_check');

        if ($this->form_validation->run() == FALSE) {
            if(!empty($siteById = $this->Sites_model->get_sites('id ='.$id))) {
                $data['name'] = $siteById->name;
                $data['url'] = $siteById->url;
            } else {
                show_404();
                exit;
            }

            $data['title'] = 'Edit sites form';
            $data['id'] = $id;
            $data['label1'] = 'Site name';
            $data['label2'] = 'Url';
            $data['route'] = 'sites/edit/'.$id;

            $this->load->view('sites/addform', $data);
        } else {
            $data = array('name' => $this->input->post('name'),
                          'url' => $this->input->post('url'),
                          'apikey' => $this->mylib->randomKey());

            if (!$this->Sites_model->edit_site($id,$data))
                show_error('Database error!');

            $this->session->set_flashdata('msg', 'Site '.$id.' has been edited');
            redirect('sites');
        }
    }

    public function name_check($str) {
        if (preg_match("/^[\s\w\.]+$/", $str)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('name_check', 'Site name cannot contain special characters!');
            return FALSE;
        }
    }

    public function url_check($str) {
        if (preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$_iuS', $str)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('url_check', 'Url doesn\'t response to URL pattern !');
            return FALSE;
        }
    }
}