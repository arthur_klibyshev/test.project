<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends BASE_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('Users_model', "", TRUE);
        $this->load->library('session');
        $this->load->helper('url');
    }
    public function showusers(){
        $data['users'] = $this->Users_model->get_users();
        $data['title'] = 'Users list';

        $this->load->view('sites/showusers', $data);
    }
    public function banuser($id) {
        if ($this->data['user_info']->role_id != ADMIN_ROLE) {
            show_404();
            exit;
        }
        $this->Users_model->ban_user($id);
        $this->session->set_flashdata('msg', 'User id#'.$id.' has been banned!');
        redirect('sites/showusers');
        exit;
    }
    public function unbanuser($id) {
        if ($this->data['user_info']->role_id != ADMIN_ROLE) {
            show_404();
            exit;
        }
        $this->Users_model->ban_user($id, 0);
        $this->session->set_flashdata('msg', 'User id#'.$id.' has been unbanned!');
        redirect('sites/showusers');
        exit;
    }
    public function edituser($id) {
        if ($this->data['user_info']->role_id != ADMIN_ROLE) {
            show_404();
            exit;
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_name', 'User name', 'trim|required|min_length[3]|max_length[50]|callback_name_check');
        $this->form_validation->set_rules('first_name', 'First name', 'trim|required|min_length[2]|max_length[20]|alpha');
        $this->form_validation->set_rules('last_name', 'Last name', 'trim|required|min_length[2]|max_length[20]|alpha');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[6]|max_length[320]|valid_email');

        if ($this->form_validation->run() == FALSE) {
            if(!empty($UserById = $this->Users_model->get_users('u.id ='.$id))) {
                    $data['user_name'] = $UserById->user_name;
                    $data['first_name'] = $UserById->first_name;
                    $data['last_name'] = $UserById->last_name;
                    $data['email'] = $UserById->email;
                    $data['role'] = $UserById->role;
            } else {
                show_404();
                exit;
            }

            $data['title'] = 'Edit users form';
            $data['id'] = $id;
            $data['label1'] = 'Login';
            $data['label2'] = 'First name';
            $data['label3'] = 'Last name';
            $data['label4'] = 'email';
            $data['label5'] = 'role';
            $data['route'] = 'sites/edituser/'.$id;

            $this->load->view('sites/edituser', $data);
        } else {
            $data = array('user_name' => $this->input->post('user_name'),
                          'first_name' => $this->input->post('first_name'),
                          'last_name' => $this->input->post('last_name'),
                          'email' => $this->input->post('email'),
                          'role' => $this->input->post('role'));

            if (!$this->Users_model->edit_user($id,$data)) {
                show_error('Database error!');
                exit;
            }

            $this->session->set_flashdata('msg', 'User '.$id.' has been edited');
            redirect('sites/showusers');
            exit;
        }
    }
}