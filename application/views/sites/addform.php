<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view('templates/header');


echo validation_errors();

echo form_open($route);?>


<h5><?= $label1 ?></h5>
<input type="text" name="name" value="<?= $name ?>" size="50" />

<h5><?= $label2 ?></h5>
<input type="text" name="url" value="<?= $url ?>" size="50" />

<div><input type="submit" value="Submit" /></div>

<?php
echo form_close();

echo '<a href="'. base_url("sites") .'">Go to sites list</a></br>';
$this->load->view('templates/footer');
?>