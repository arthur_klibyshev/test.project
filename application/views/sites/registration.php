<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view('templates/header');


echo validation_errors();

echo form_open($route);?>

    <h5>Login</h5>
    <input type="text" name="user_name" value="<?= $user_name ?>" size="50" />

    <h5>First name</h5>
    <input type="text" name="first_name" value="<?= $first_name ?>" size="50" />

    <h5>Last name</h5>
    <input type="text" name="last_name" value="<?= $last_name ?>" size="50" />

    <h5>Password</h5>
    <input type="text" name="password" value="<?= $password ?>" size="50" />

    <h5>Password confirmation</h5>
    <input type="text" name="pass_confirm" value="<?= $pass_confirm ?>" size="50" />

    <h5>Email</h5>
    <input type="text" name="email" value="<?= $email ?>" size="50" />

<div><input type="submit" value="Submit" /></div>

<?php
echo form_close();

echo '<a href="'. base_url("sites/login") .'">Login</a></br>';
$this->load->view('templates/footer');
?>