<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view('templates/header');

echo $this->session->flashdata('msg');

echo 'Hello '. $usermsg->first_name .' '. $usermsg->last_name.'!';

echo '<table>';
if (!empty($sites)) {
    foreach ($sites as $value) {
        echo '<tr>';
        echo '<td>' . $value->id . '</td>
          <td>' . $value->name . '</td>
          <td>' . $value->url . '</td>
          <td><a href="' . base_url("sites/delete/" . $value->id) . '">Delete</a></td>
          <td><a href="' . base_url("sites/edit/" . $value->id) . '">Edit</a></td>';
        echo '</tr>';
    }
}
echo '</table>';

echo '<a href="'. base_url("sites/add") .'">Add site</a></br>';

$this->load->view('templates/footer');