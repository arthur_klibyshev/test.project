<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view('templates/header');


echo validation_errors();

echo form_open($route);?>


    <h5><?= $label1 ?></h5>
    <input type="text" name="user_name" value="<?= $user_name ?>" size="50" />

    <h5><?= $label2 ?></h5>
    <input type="text" name="first_name" value="<?= $first_name ?>" size="50" />

    <h5><?= $label3 ?></h5>
    <input type="text" name="last_name" value="<?= $last_name ?>" size="50" />

    <h5><?= $label4 ?></h5>
    <input type="text" name="email" value="<?= $email ?>" size="50" />

    <h5><?= $label5 ?></h5>
    <select name="role">
        <option <?= ($role == 'Admin')?'selected':'' ?> value="1">Admin</option>
        <option <?= ($role == 'Moderator')?'selected':'' ?> value="2">Moderator</option>
        <option <?= ($role == 'User')?'selected':'' ?> value="3">User</option>
    </select>

<div><input type="submit" value="Submit" /></div>

<?php
echo form_close();

echo '<a href="'. base_url("sites/showusers") .'">Go to users list</a></br>';
$this->load->view('templates/footer');
?>