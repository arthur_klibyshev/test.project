<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view('templates/header');

echo $this->session->flashdata('msg');
echo '<table>';
if (!empty($users)) {
    foreach ($users as $value) {
        echo '<tr>';
        echo '<td>' . $value->id . '</td>
              <td>' . $value->user_name . '</td>
              <td>' . $value->email . '</td>
              <td>' . $value->role. '</td>';
        if ($this->data['user_info']->role_id == ADMIN_ROLE) {
            echo '<td><a href="' . base_url("sites/" . (($value->banned)? 'unbanuser/':'banuser/') . $value->id) . '">' . (($value->banned)? 'Unban':'Ban') .'</a></td>';
            echo '<td><a href="' . base_url("sites/edituser/" . $value->id) . '">Edit</a></td>';
        }
        echo '</tr>';
    }
}
echo '</table>';

$this->load->view('templates/footer');