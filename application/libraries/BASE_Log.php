<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.08.15
 * Time: 16:34
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BASE_Log extends CI_Log {

    public function __construct()
    {
        parent::__construct();

        //updated log levels according to the correct order
        $this->_levels	= array('ERROR' => '1', 'INFO' => '2', 'DEBUG' => '3', 'ALL' => '4');
    }

}