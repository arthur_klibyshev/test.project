<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mylib {

    public function randomKey( $length=10 ) {
        $chars = implode('', range(0, 9)). implode('', range('a', 'z')). implode('', range('A', 'Z'));

        $code = "";

        $clen = strlen($chars) - 1;
        while (strlen($code) <= $length) {
            $code .= $chars[mt_rand(0,$clen)];
        }

        return $code;
    }
}