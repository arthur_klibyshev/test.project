<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sites_model extends CI_Model {

    public function get_sites($where = '') {
        if(!empty($where)){
            $where = " WHERE $where";
        }
        $result = $this->db->query("SELECT id, name, url, apikey FROM sites $where")->result();
        return (!empty($result)) ? ($where) ? reset($result) : $result : false;

    }

    public function add_site($data) {
        $this->db->query("INSERT INTO sites (NAME, url, apikey)
                          VALUES (".$this->db->escape($data['name']).", ". $this->db->escape($data['url']) .", '".$data['apikey']."')");
        return $this->db->insert_id();
    }

    public function delete_site($id) {
        $query = $this->db->query('SELECT * FROM sites WHERE id='.$id)->result();
        if(empty($query)){
            show_404();
            exit;
        }
        $query = $this->db->query('DELETE FROM sites WHERE id='.$id);
        if ($this->db->affected_rows()) {
            return TRUE;
        }
        return FALSE;
    }

    public function edit_site($id, $data) {
        $query = $this->db->query('SELECT * FROM sites WHERE id='.$id)->result();
        if(empty($query)){
            show_404();
            exit;
        }
        $this->db->trans_begin();
        $query = $this->db->query("UPDATE sites SET name = ".$this->db->escape($data['name']).", url = ".$this->db->escape($data['url'])." WHERE	id = ".$id);
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return TRUE;
    }
}