<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function add_user($data){
        $this->db->trans_begin();
        $this->db->query("INSERT INTO users (user_name, password, first_name, last_name, email, banned)
                          VALUES (". $this->db->escape($data['user_name']) .",
                                  ". $this->db->escape($data['password']) .",
                                  ". $this->db->escape($data['first_name']) .",
                                  ". $this->db->escape($data['last_name']) .",
                                  ". $this->db->escape($data['email']) .",
                                  ". $data['banned'] ."
                                  )");
        $this->db->query("INSERT INTO users_has_roles (role_id, user_id) VALUES ('".$data['role']."',@@IDENTITY)");

        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return TRUE;
    }

    public function  get_users($where = ''){
        if(!empty($where)){
            $where = " WHERE $where ";
        }
        $result = $this->db->query("
            SELECT u.id, u.user_name,u.first_name, u.last_name, u.email, r.role, u.banned, uhr.role_id FROM users u
            JOIN users_has_roles uhr ON u.id = uhr.user_id
            JOIN roles r ON uhr.role_id = r.id
            $where
            ORDER BY u.id")->result();
        return (!empty($result)) ? ($where) ? reset($result) : $result : false;
    }

    public function ban_user($id, $mode = 1){
        $query = $this->db->query('SELECT id FROM users WHERE id='.$id)->result();
        if(empty($query)){
            show_404();
            exit;
        }
        $query = $this->db->query("UPDATE users SET banned = ". $mode ." WHERE id = ".$id);
        if ($this->db->affected_rows()) {
            return TRUE;
        }
        return FALSE;
    }

    public function edit_user($id, $data){
        $query = $this->db->query('SELECT * FROM users WHERE id='.$id)->result();
        if(empty($query)){
            show_404();
            exit;
        }
        $this->db->trans_begin();
        $query = $this->db->query("UPDATE users SET user_name = ".$this->db->escape($data['user_name']).",
                                                    first_name = ".$this->db->escape($data['first_name']).",
                                                    last_name = ".$this->db->escape($data['last_name']).",
                                                    email = ".$this->db->escape($data['email'])."
                                                    WHERE id = ".$id);
        $query = $this->db->query("UPDATE users_has_roles SET role_id = '". $data['role'] ."' WHERE user_id = ". $id);
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return TRUE;
    }

}