<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class BASE_Controller extends CI_Controller {
    
    public function __construct() {
        
        parent::__construct();
        //$this->load->model('Posts', '', TRUE);
        //$this->load->model('Users', '', TRUE);
        //$this->load->model('Post_as_categories', '', TRUE);
        //$this->load->model('Categories', '', TRUE);
        $this->load->library('session');
        $this->load->helper('url');

            if($this->session->userdata('user_authorization') == false){
                if(!empty($this->input->get('code'))){
                    $this->session->set_userdata('code',$this->input->get('code')); 
                }
                redirect('sites/login');
                exit;
            }
            
            $this->data['user_info'] = $this->session->userdata('user_data');     

        }

}

